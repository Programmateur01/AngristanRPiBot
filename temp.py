#!/usr/bin/env python
# coding: utf-8
import os
from twython import Twython

import mytoken

api = Twython(mytoken.CONSUMER_KEY, mytoken.CONSUMER_SECRET, mytoken.ACCESS_KEY, mytoken.ACCESS_SECRET)

cmd = '/opt/vc/bin/vcgencmd measure_temp'
line = os.popen(cmd).readline().strip()
temp = line.split('=')[1].split("'")[0]
api.update_status(status='La température du CPU est de '+temp+' °C')
