#!/usr/bin/env python
import sys
from twython import Twython

import mytoken

api = Twython(mytoken.CONSUMER_KEY, mytoken.CONSUMER_SECRET, mytoken.ACCESS_KEY, mytoken.ACCESS_SECRET)

#api.update_status(status=sys.argv[1:])
api.update_status(status=' '.join(map(str, sys.argv[1:])))
